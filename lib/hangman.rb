# frozen_string_literal: true

class Hangman
  require 'json'
  require_relative 'player.rb'

  @@DICTIONARY_FILE = '5desk.txt'

  def initialize
    show_starting_screen

    if load_game?
      load_game
    else
      setup_game
    end
  end

  public

  def to_s
    'hangman'
  end

  def setup_game
    create_player
    initialize_game
    @turn_count = 0
    @incorrect_letters_guessed = []
    @save_game = false
  end

  def play_next_turn
    puts '*******************************'
    @turn_count += 1
    puts "Turn #{@turn_count}!"
    puts "Guesses left: #{@attempts_remaining}"
    display_clue
    player_guess = get_player_guess
    if player_guess == 'save'
      @save_game = true
      return
    end
    check_player_guess(player_guess)
  end

  def game_over?
    return true if @attempts_remaining == 0 || !(@clue.include? '_')
    false
  end

  def save_game?
    @save_game
  end

  def save_game
    Dir.mkdir('saves') unless Dir.exist?('saves/')
    filename = create_savefile_name
    file = File.new(filename, 'w')
    file.puts to_json
    file.close
  end

  def load_game?
    user_input = ''
    while user_input != 'Y' && user_input != 'N'
      puts 'Do you want to load a saved game? [Y/N]'
      user_input = gets.chomp.upcase
    end
    user_input == 'Y'
  end

  def load_game
    if Dir.exist?('saves/')
      from_json(read_savefile)
      @loaded_filename = save_file_name
    else
      puts 'No save files exist!'
      setup_game
    end
  end

  def declare_winner
    puts '*******************************'
    display_clue

    if @clue.include? '_'
      puts "You lose #{@player}! :( The word was #{@secret_word}"
    else
      puts "You win #{@player}! :D"
    end
  end

  def to_json(*_args)
    JSON.dump ({
      turn_count: @turn_count,
      incorrect_letters_guessed: @incorrect_letters_guessed,
      secret_word: @secret_word,
      attempts_remaining: @attempts_remaining,
      clue: @clue,
      player: @player
    })
  end

  def from_json(string)
    data = JSON.load string
    @turn_count = data['turn_count'].to_i - 1
    @incorrect_letters_guessed = data['incorrect_letters_guessed']
    @secret_word = data['secret_word']
    @attempts_remaining = data['attempts_remaining']
    @clue = data['clue']
    @player = Player.new(data['player'])
end

  private

  def show_starting_screen
    puts '*******************************'
    puts '*   Hangman by Alain Suarez   *'
    puts '*******************************'
    puts '* Try to guess a word in a    *'
    puts '* limited number of guesses!  *'
    puts '* Guess one letter at a time. *'
    puts '*******************************'
  end

  def create_player
    puts 'What is your name?'
    @player = Player.new(gets.chomp.capitalize)
  end

  def initialize_game
    set_attempts_remaining
    create_secret_word
    create_clue(@secret_word)
  end

  def set_attempts_remaining
    puts "In how many attempts can you figure out the word #{@player}?"
    @attempts_remaining = ''
    while @attempts_remaining !~ /^[1-9][0-9]*$/
      puts 'Enter a number of guess attempts:'
      @attempts_remaining = gets.chomp
    end
    @attempts_remaining = @attempts_remaining.to_i
  end

  def create_secret_word
    lines = File.readlines @@DICTIONARY_FILE
    candidate_words = lines.select { |line| line.length > 4 && line.length < 13 }
    @secret_word = candidate_words.sample
  end

  def create_clue(secret_word)
    @clue = ''
    secret_word.strip.each_char do |_char|
      @clue = +@clue << '_ ' # needed + to circumvent all strings frozen
    end
  end

  def get_player_guess
    puts "Guess a letter #{@player}: (or enter 'save' to save and quit)"
    guess = gets.chomp
    return 'save' if guess.downcase == 'save'

    while guess !~ /^[a-zA-Z]{1}$/ || check_previously_guesssed(guess)
      puts 'Enter a letter:'
      guess = gets.chomp
      return 'save' if guess.downcase == 'save'
    end
    guess
  end

  def check_previously_guesssed(player_guess)
    (@incorrect_letters_guessed.include? player_guess) || (@clue.include? player_guess)
  end

  def check_player_guess(player_guess)
    if @secret_word.downcase.include? player_guess.downcase
      parse_guess(player_guess)
    else
      @incorrect_letters_guessed << player_guess.downcase
      @attempts_remaining -= 1
    end
  end

  def parse_guess(player_guess)
    clue_array = @clue.split(' ')
    @secret_word.strip.downcase.split('').each_with_index do |char, index|
      clue_array[index] = char if char == player_guess.downcase
    end
    @clue = clue_array.join(' ')
  end

  def display_clue
    puts @clue
    puts "Not in word: #{@incorrect_letters_guessed.join(',')}"
  end

  def choose_save_file
    files = []
    Dir.foreach('saves/') do |file|
      next if (file == '.') || (file == '..')

      files << file[0..-6].downcase
    end

    file_name = ''
    until files.include? file_name
      puts 'Save files:'
      puts files
      puts 'Choose a save by name:'
      file_name = gets.chomp
    end
    file_name
  end

  def create_savefile_name
    if @loaded_filename.nil?
      filename = "saves/#{@player}.json"
      counter = 1
      while File.exist?(filename)
        filename = "saves/#{@player}#{counter}.json"
        counter += 1
      end
    else
      filename = "saves/#{@loaded_filename}.json"
      end
    filename
  end

  def read_savefile
    save_file_name = choose_save_file
    save_file = File.new('saves/' + save_file_name.capitalize + '.json', 'r')
    contents = save_file.read
    save_file.close
    contents
  end
end
