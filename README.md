# ruby-hangman

Created with Ruby as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/ruby-programming/lessons/file-i-o-and-serialization). 

# Final Thoughts

Completing this project allowed me to practice with Files, Serialization and Object Oriented Programming in Ruby.